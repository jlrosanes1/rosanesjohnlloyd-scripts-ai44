#!/bin/bash

read -p  "What is the name of your folder?: " FOLDERNAME

read -p  "How many copies do you want?: " COPIES

for (( i = 1; i <= $COPIES; i++ ))
do
    if (( $i == 1 ))
    then
        mkdir $FOLDERNAME;
    else
        mkdir $FOLDERNAME$i;
    fi
done
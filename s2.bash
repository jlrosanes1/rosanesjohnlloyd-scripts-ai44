#!/bin/bash

read -p  "Enter your last name: " LASTNAME

read -p  "Enter your first name: " FIRSTNAME

read -p  "Enter your birthdate in yyyy-mm-dd format: " BIRTHDATE

USERBIRTHDATE=$(date -d "$BIRTHDATE" +%Y-%m-%d)
NOW=$(date +%Y-%m-%d)


NOWYEAR=$(date +%Y)
NOWMONTH=$(date +%m)

USERYEAR=$(date -d "$USERBIRTHDATE" +%Y)
USERMONTH=$(date -d "$USERBIRTHDATE" +%m)

age=$(($NOWYEAR - $USERYEAR))
month=$(($NOWMONTH - $USERMONTH))

if [[ $month -lt 0 || ( $month -eq 0 &&  $NOW -lt $USERBIRTHDATE ) ]]
then
    age=$(( $age - 1))
fi


echo "Hello, $FIRSTNAME $LASTNAME! You're ${age} years old!"
